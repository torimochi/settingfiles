;;;;; Load path setting
(let ((default-directory (expand-file-name "~/.emacs.d/elpa")))
	(add-to-list 'load-path default-directory)
	(if (fboundp 'normal-top-level-add-subdirs-to-load-path)
			(normal-top-level-add-subdirs-to-load-path)))

(package-initialize)
(add-to-list 'package-archives '("melpa" .
	       "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("melpa-stable" .
	       "http://melpa-stable.milkbox.net/packages/"))

;;;;; Color theme
;(color-theme-initialise)
;(color-theme-ld-dark)


;;;;; Language setting
(set-locale-environment nil) ;

;;;;; key bind settings
(define-key global-map "\C-h" 'delete-backward-char) ; delete
(define-key global-map "\C-z" 'undo) ;undo

;;;;; parenthethis
(show-paren-mode 1)

;;;;; use wheel mouse
;(mouse-wheel-mode t)
;(setq mouse-wheel-follow-mouse t)

;;;;;
;(show-paren-mode 1)
;(setq show-paren-style 'mixed)

;;;;; Do not make backup file
(setq backup-inhibited t)
(setq delete-auto-save-files t)

;;;;; Show cursor position
(column-number-mode t)
(line-number-mode t)

;;;;; insert newline at final
(setq require-final-newline t)

;;;;; auto break line
(setq fill-column 80)
(setq-default auto-fill-mode t)

;;;;; set default tab width
(setq default-tab-width 2)

;;;;; using twittering-mode
;(add-to-list 'load-path "~/.emacs.d/twittering-mode-2.0.0")
;(require 'twittering-mode)
;(require 'w32-shell-execute)
;;;;; set function mode
